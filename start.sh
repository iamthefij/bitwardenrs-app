#!/bin/bash

set -eu

echo "=> Exporting env vars expected by Bitwarden"
export SIGNUPS_ALLOWED=${SIGNUPS_ALLOWED:-false}
export INVITATIONS_ALLOWED=${INVITATIONS_ALLOWED:-true}
export DOMAIN=$CLOUDRON_APP_ORIGIN
export SMTP_HOST=$CLOUDRON_MAIL_SMTP_SERVER
export SMTP_FROM=$CLOUDRON_MAIL_FROM
export SMTP_FROM_NAME="${SMTP_FROM_NAME:-Bitwarden}"
export SMTP_PORT=$CLOUDRON_MAIL_SMTP_PORT
export SMTP_SSL=false
export SMTP_EXPLICIT_TLS=false
export SMTP_USERNAME=$CLOUDRON_MAIL_SMTP_USERNAME
export SMTP_PASSWORD=$CLOUDRON_MAIL_SMTP_PASSWORD
export SMTP_AUTH_MECHANISM="Plain"
export SMTP_TIMEOUT=15
export DATABASE_URL=$CLOUDRON_MYSQL_URL
export ENABLE_DB_WAL=false
export LOG_LEVEL=${LOG_LEVEL:-debug}

# Generate admin token if it doesn't exist
if [[ ! -f /app/data/admin_token ]]; then
    pwgen -1 48 -s > /app/data/admin_token
fi
export ADMIN_TOKEN=$(cat /app/data/admin_token)
echo "=> Admin token: ${ADMIN_TOKEN}"

if [ -z "${CLOUDRON_LDAP_SERVER}" ]; then
    echo "=> SSO integration disabled. No LDAP config to generate"
else
    echo "=> Generate LDAP config"
    # Generate ldap sync config from template
    sed -e "s/##LDAP_SERVER/${CLOUDRON_LDAP_SERVER}/"\
        -e "s/##LDAP_PORT/${CLOUDRON_LDAP_PORT}/"\
        -e "s/##LDAP_USERS_BASE_DN/${CLOUDRON_LDAP_USERS_BASE_DN}/"\
        -e "s/##LDAP_BIND_DN/${CLOUDRON_LDAP_BIND_DN}/"\
        -e "s/##LDAP_BIND_PASSWORD/${CLOUDRON_LDAP_BIND_PASSWORD}/"\
        -e "s/##BITWARDEN_HOSTNAME/${CLOUDRON_APP_HOSTNAME}/"\
        -e "s/##ADMIN_TOKEN/${ADMIN_TOKEN}/"\
        /app/code/ldap_config.template.toml > /run/ldap_config.toml
fi

echo "=> Starting supervisord"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Bitwarden
