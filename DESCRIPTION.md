Bitwarden is a self-hosted password manager. It allows you to store and manage your passwords, credit cards, and other private information in a secure way while still allowing you to access it from your browser, phone, or desktop.

Note on User Management: **Bitwarden does not support Single Sign On**. This is by design for security reasons. You must create a new password for your Bitwarden account.

In addition, all users must be invited to your Bitwarden instance to sign up. By default it will not allow open registration. If you wish to automatically invite users, you may select the Cloudron user management, though you should keep in mind that it *will not* fully manage your users. It will only send them an invite email. No more, no less.

If you use Cloudron for user management: Any users with access should automatically receive an invite and be able to register for their own accounts. They will need to create a new password for this application due to client side encryption requirements. You can manage the server through the admin interface at `/admin`. Users access will not be revoked. You must manually revoke access through the admin interface if you wish to do so.

If you do not use Cloudron for user management: On initial creation, no users will have access to the server. You must visit `/admin` and invite any users that you wish to have access.
