#! /bin/bash
set -e

export ADMIN_TOKEN=$(cat /app/data/admin_token)

if [ -z "${CLOUDRON_LDAP_SERVER}" ]; then
    echo "=> SSO integration disabled. Skip LDAP invites"
else
    echo "=> Generate LDAP config"
    # Generate ldap sync config from template
    sed -e "s/##LDAP_SERVER/${CLOUDRON_LDAP_SERVER}/"\
        -e "s/##LDAP_PORT/${CLOUDRON_LDAP_PORT}/"\
        -e "s/##LDAP_USERS_BASE_DN/${CLOUDRON_LDAP_USERS_BASE_DN}/"\
        -e "s/##LDAP_BIND_DN/${CLOUDRON_LDAP_BIND_DN}/"\
        -e "s/##LDAP_BIND_PASSWORD/${CLOUDRON_LDAP_BIND_PASSWORD}/"\
        -e "s/##BITWARDEN_HOSTNAME/${CLOUDRON_APP_HOSTNAME}/"\
        -e "s/##ADMIN_TOKEN/${ADMIN_TOKEN}/"\
        /app/code/ldap_config.template.toml > /run/ldap_config.toml

    export CONFIG_PATH=/run/ldap_config.toml
    exec /app/code/bitwarden_rs_ldap
fi
