# bitwardenrs-app

A Cloudron deployment for [Bitwarden_rs](https://github.com/dani-garcia/bitwarden_rs) that includes MySQL support and some basic integration with LDAP for automated invites.

## Notes on LDAP integration

LDAP integration will not include SSO. This is by design from Bitwarden_rs ([context](https://github.com/dani-garcia/bitwarden_rs/pull/677)). The short of it is that it can not be done in a secure way without breaking the client apps.

If you enable SSO (again, not _really_ SSO) when installing, the application will automatically send an invite email to any user that is scoped for access to Bitwarden. Only those users will be able to sign up unless you manually invite more via the admin panel.

If you do not enable SSO, you will have to manually invite all users via the Admin panel.

Alternately, if you have the ability to customize your environment variables, you can whitelist a particular email domain for signups. I believe at some point this will be part of Cloudron and may not require manual updates to the database.

## Building

### The Dockerfiles

The main `Dockerfile` in this repo is actually a symblink pointing to a proper `Dockerfile` that will be used for building.

There are two real Dockerfiles in this repo. One pulls Bitwarden_rs from Docker Hub and extracts the binary. The other will actually build it from source. It used to be required to build from source for MySQL integration, but it is no longer necessary. Should you wish to have more control over the build process, or enable any other build time features in the future, you should use `Dockerfile.multi-stage`. You can easily switch between them by changing the symblink for `Dockerfile`.

### The build process

A `Makefile` has been added to simplify the interaction with Cloudron. More detailed instructions exist within the `Makefiel` itself, but to build, push, and install on your Cloudron you should only need to do `make push install` or `make push update`.

You will have to be previously authenticated with both Docker Hub and your Cloudron instance via the Cloudron CLI.
