# Description: This Makefile is designed to simplify the building and installation 
# of a Cloudron app. It should make development and testing a bit easier.
#
# Author: Ian Fijolek <iamthefij>
#
# Building requirements:
# 	* Docker
# 	* Cloudron CLI
# 	* A Docker registry to push to (eg. hub.docker.com)
# 	* Recommended: jq to automatically set the version number
#
# Installation requirements:
# 	* Cloudron CLI
#
# Typical usage:
#
# For an App Creator:
# 1. Make a copy of this file and put it in a new directory. 
# 2. Update the variables at the top of this file to reference your app
# 	and repo names.
# 3. Run `make init` to create your Cloudron app.
# 4. To build and install, run `make push install`
# 	If you encounter an error pushing, make sure you're logged into your registry.
# 5. To build and update, run `make push update`
# 6. (Optional) If you're using a private repo or a variable set at 
# 	runtime (eg `$(USER)`), in your readme describe what env variables should
# 	be set for end users to install. Eg.
# 		To install, use `env CLOUDRON_APP=<name you want to use> DOCKER_REPO=iamthefij make install
#
# For an End User:
# 1. Check the Readme file for more specific instructions.
# 2. If no instructions found, try the following:
# 	`env CLOUDRON_APP=<name you want to use> make install
# 	`env CLOUDRON_APP=<name you want to use> DOCKER_REPO=<developers name> make install

# Variables

# The subdomain to install the app on.
# This can be overriden by setting the variable when running
# `make`. Eg. `env CLOUDRON_APP=pass make install`.
CLOUDRON_APP ?= pass
# Registery that images should be pushed to
DOCKER_REGISTRY ?= docker.io
# Repo on provided registery that should be pushed to
# This will use a repo named after the current user. It could also be
# hardcoded here by replacing `$(USER)` with something like `iamthefij`.
# Additionally, this can be overriden by setting the variable when running
# `make`. Eg. `env DOCKER_REPO=iamthefij make push`.
DOCKER_REPO ?= $(USER)
# The base image tag name to use. This should be the name of the app.
DOCKER_TAG := cloudron-app-bitwarden
# The latest version that this should be tagged as.
# This should equal whatver is in your `CloudronManifest.json` file. If you
# do not have `jq` installed, you should hard code this value.
VERSION := $(shell jq -r .version CloudronManifest.json || echo 'latest')
# Full registry/repo/tag. This is used for pushing and pulling.
DOCKER_FULL := $(DOCKER_REGISTRY)/$(DOCKER_REPO)/$(DOCKER_TAG)

# Make targets

.PHONY: init
init:
	cloudron init

.PHONY: default
default: build

# Builds a new docker image using the tag name defined in a simplified manner
.PHONY: build
build:
	docker build . -t $(DOCKER_TAG)

# Tags already built docker image with the docker repo and versions for pushing
.PHONY: tag
tag: build
	docker tag $(DOCKER_TAG) $(DOCKER_REPO)/$(DOCKER_TAG):latest
	docker tag $(DOCKER_TAG) $(DOCKER_REPO)/$(DOCKER_TAG):$(VERSION)

# Pushes tagged images to the provided registry
.PHONY: push
push: tag
	docker push $(DOCKER_FULL):latest
	docker push $(DOCKER_FULL):$(VERSION)

# Install the app on your Cloudron instance
.PHONY: install
install:
	cloudron install --app $(CLOUDRON_APP) --image $(DOCKER_FULL):latest

# Update the app on your Cloudron instance
.PHONY: update
update:
	cloudron update --app $(CLOUDRON_APP) --image $(DOCKER_FULL):latest
